#!/bin/sh

# Tool to convert DOS text files (IBM437 encoding, CRLF line endings)
# to modern Unix UTF-8 encoded file

if [ ! -f "$1" ]; then
	echo "Usage: $0 <file> ... [filen]"
	exit 1;
fi

tmpfoo=$(basename $0)
tmpfile=$(mktemp -t ${tmpfoo})

while [ ! -z "$1" ] ; do
	cp -v "$1" "${tmpfile}" || exit 1
	dos2unix "${tmpfile}"
	iconv -f IBM437 "${tmpfile}" | sed 's/[ ]*$//' > $1
	echo "$1 converted"
	shift
done

rm "${tmpfile}"
